// JavaScript Document


	/*----------------------------------------------------*/
	/*	Preloader
	/*----------------------------------------------------*/

    $(window).load(function() {
	
		"use strict";	
	
       $('#status').delay(100).fadeOut('slow');
       $('#preloader').delay(500).fadeOut('slow');
       $('body').delay(500).css({'overflow':'visible'});
	   
	});
	

	/*----------------------------------------------------*/
	/*	Parallax
	/*----------------------------------------------------*/
	$(window).bind('load', function() {
	
		"use strict";	
		parallaxInit();
		
	});

	function parallaxInit() {
		$('#intro').parallax("10%", 0.3);
		$('#features').parallax("10%", 0.3);
		$('#call-to-action').parallax("30%", 0.3);	
	}
	
	
	/*----------------------------------------------------*/
	/*	Accordion
	/*----------------------------------------------------*/
	
	$(document).ready(function(){
	
		"use strict";

		$('ul.accordion').accordion();
		
	});
	
	
	/*----------------------------------------------------*/
	/*	Carousel
	/*----------------------------------------------------*/
	
	$(document).ready(function(){

		"use strict";
				
		$("#our-customers").owlCarousel({
					  
			slideSpeed : 600,
			items : 6,
			itemsDesktop : [1199,5],
			itemsDesktopSmall : [960,4],
			itemsTablet: [768,3],
			itemsMobile : [480,2],
			navigation:true,
			pagination:false,
			navigationText : false
					  
		});
				
		// Carousel Navigation
		$(".next").click(function(){
			$("#our-customers").trigger('owl.next');
		})
		
		$(".prev").click(function(){
			$("#our-customers").trigger('owl.prev');
		})

		
	});	
	
	
	/*----------------------------------------------------*/
	/*	Flexslider
	/*----------------------------------------------------*/
	
	$(document).ready(function(){
	
		"use strict";

		$('.flexslider').flexslider({
			animation: "fade",
			controlNav: false,
			directionNav: false,  
			slideshowSpeed: 6000,   
			animationSpeed: 800,  
			start: function(slider){
				$('body').removeClass('loading');
			}
		});

	});
	
	
	/*----------------------------------------------------*/
	/*	ScrollUp
	/*----------------------------------------------------*/
	/**
	* scrollUp v1.1.0
	* Author: Mark Goodyear - http://www.markgoodyear.com
	* Git: https://github.com/markgoodyear/scrollup
	*
	* Copyright 2013 Mark Goodyear
	* Licensed under the MIT license
	* http://www.opensource.org/licenses/mit-license.php
	*/

	$(document).ready(function(){

		'use strict';

		$.scrollUp = function (options) {

			// Defaults
			var defaults = {
				scrollName: 'scrollUp', // Element ID
				topDistance: 600, // Distance from top before showing element (px)
				topSpeed: 1200, // Speed back to top (ms)
				animation: 'fade', // Fade, slide, none
				animationInSpeed: 200, // Animation in speed (ms)
				animationOutSpeed: 200, // Animation out speed (ms)
				scrollText: '', // Text for element
				scrollImg: false, // Set true to use image
				activeOverlay: false // Set CSS color to display scrollUp active point, e.g '#00FFFF'
			};

			var o = $.extend({}, defaults, options),
				scrollId = '#' + o.scrollName;

			// Create element
			$('<a/>', {
				id: o.scrollName,
				href: '#top',
				title: o.scrollText
			}).appendTo('body');
			
			// If not using an image display text
			if (!o.scrollImg) {
				$(scrollId).text(o.scrollText);
			}

			// Minium CSS to make the magic happen
			$(scrollId).css({'display':'none','position': 'fixed','z-index': '2147483647'});

			// Active point overlay
			if (o.activeOverlay) {
				$("body").append("<div id='"+ o.scrollName +"-active'></div>");
				$(scrollId+"-active").css({ 'position': 'absolute', 'top': o.topDistance+'px', 'width': '100%', 'border-top': '1px dotted '+o.activeOverlay, 'z-index': '2147483647' });
			}

			// Scroll function
			$(window).scroll(function(){	
				switch (o.animation) {
					case "fade":
						$( ($(window).scrollTop() > o.topDistance) ? $(scrollId).fadeIn(o.animationInSpeed) : $(scrollId).fadeOut(o.animationOutSpeed) );
						break;
					case "slide":
						$( ($(window).scrollTop() > o.topDistance) ? $(scrollId).slideDown(o.animationInSpeed) : $(scrollId).slideUp(o.animationOutSpeed) );
						break;
					default:
						$( ($(window).scrollTop() > o.topDistance) ? $(scrollId).show(0) : $(scrollId).hide(0) );
				}
			});

			// To the top
			$(scrollId).click( function(event) {
				$('html, body').animate({scrollTop:0}, o.topSpeed);
				event.preventDefault();
			});

		};
		
		$.scrollUp();

	});
	
	
	/*----------------------------------------------------*/
	/*	Register Form Validation
	/*----------------------------------------------------*/
	
	function getUtmSources() { 
		var url = location.href;
		var arr = new Array();
		arr = url.split("?");
		if (arr.length > 1) {
			var parse_url = arr[arr.length-1];
			var parse_url = parse_url.split('&');
			return parse_url;
		}
	}
	
	function trackConv(google_conversion_id, google_conversion_label) {
		var image = new Image(1, 1);
		image.src = "//www.googleadservices.com/pagead/conversion/" + google_conversion_id + "/?label=" + google_conversion_label;
	}
	
	$(document).ready(function(){	
		
		"use strict";
		
		var server = 'https://api.campusenrol.works/v1/remote';

		var parsed_data = getUtmSources();	
	
		$('#user_data form').validator().on('submit', function (e) {
			if (e.isDefaultPrevented()) {
			
			} else {
				var $btn = $('#user_data form input.btn').button('loading');
				var serialized_data = $('#user_data form').serialize();
					if (parsed_data != undefined) {
						for(var i=0;i<parsed_data.length;i++) {
							serialized_data += '&' + parsed_data[i];
						}
					}
				$.ajax({
					type		:	'POST',
					url			:	server,
					headers		: {key:'5bsDCVYywy4efVKR574iYXn2UMpmDs'},
					crossDomain	: 	true,
					data		: 	serialized_data,
					success		: 	function() {
										$btn.button('reset');
										$('#user_data').hide(0);
										$('#verify_otp').show(0);
										$('#verify_otp input[name=phone]').val($('#user_data input[name=phone]').val());
										$('#download_brochure').attr('href', 'https://tulas.edu.in/wp-content/uploads/2017/05/Tulas-Prospectus-2017-1.pdf');
										trackConv(959625469, 'tBR5CI-AsG4Q_fHKyQM');
									},
					error		:	function() {}
				});
				
				return false;
			}
		});
			

		var laedverify = "https://api.campusenrol.works/v1/remote/verify-mobile";

		$('#verify_otp form').validator().on('submit', function (e) {
			if (e.isDefaultPrevented()) {

			} else {
				var $btn = $('#verify_otp form input.btn').button('loading');
				$.ajax({
					type		:	'POST',
					url			:	laedverify,
					headers		: {key:'5bsDCVYywy4efVKR574iYXn2UMpmDs'},
					crossDomain	: 	true,
					data		: 	$('#verify_otp form').serialize(),
					success		: 	function(data) {
						$btn.button('reset');
						console.log(data);
						if (data == '1') {
							$('#verify_otp').hide(0);
							$('#thank_you').show(0);
						} else {
							$('#verify_otp input[name=otp]').parents('.form-group').addClass('has-error');
							$('#verify_otp input[name=otp]').next('.help-block').html('<ul class="list-unstyled"><li>Incorrect OTP</li></ul>');
						}
					},
					error		:	function() {}
				});

				return false;
			}
		});
	});

	