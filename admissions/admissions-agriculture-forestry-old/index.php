<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->

	<head>
	
		<!-- Basic -->
		<meta charset="utf-8">
		<title>Best Agriculture & Foestry college in Dehradun | No-1 College Uttarakhand</title>
		<meta name="description" content="Tula’s is one of the best Agriculture & Foestry college in Dehradun,uttarakhand. Get details of placement, ranking, Faculties, Facilities.">		
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">		
			
		<link href="css/bootstrap.css" rel="stylesheet"> 		
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/flexslider.css" rel="stylesheet">
		<link href="css/owl.carousel.css" rel="stylesheet">	
		<link href="css/animate.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">  
		<link href="css/responsive.css" rel="stylesheet"> 
									
		<link href='http://fonts.googleapis.com/css?family=Lato:400,900italic,900,700italic,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>
										
			
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1852404014975914'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1852404014975914&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->			

<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?1i4gkNX0idY8gecWrjUiYcilUf4WhbDe';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
	
	</head>
		
	<body>
	
	
		<!-- CONTENT WRAPPER
		============================================= -->
		
			<div id="content-wrapper">
			
			<!-- PRELOADER
			============================================= -->
					
			<div id="preloader">
				<div id="status"></div>
			</div>
		
			<!-- INTRO
			============================================= -->
			
			<section id="intro" class="intro-parallax">
				<div class="container">				
				
					<!-- Header -->		
					<header id="header"> 
						<div class="row">	

							<!-- Logo Image -->
							<div id="logo_image" class="col-md-6">
								<a href="#intro"><img src="img/logo.png" alt="logo" role="banner"></a>						
							</div>
						
							<!-- Header Social Icons -->
							<div id="social_icons" class="col-md-6 text-right">																	
								<ul class="social-icons clearfix">
									<li><a class="he_social ico-facebook" href="https://www.facebook.com/Tulas.Institute.Dehradun/" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a class="he_social ico-twitter" href="https://twitter.com/Tulas_Dehradun" target="_blank"><i class="fa fa-twitter"></i></a></li>	
									<li><a class="he_social ico-google-plus" href="https://plus.google.com/u/0/b/117590320289532314094/109874494989346734056" target="_blank"><i class="fa fa-google-plus"></i></a></li>							
									<li><a class="he_social ico-linkedin" href="http://www.linkedin.com/company/1671832" target="_blank"><i class="fa fa-linkedin"></i></a></li>
									<li><a class="he_social ico-instagram" href="https://www.instagram.com/tulasdehradun/" target="_blank"><i class="fa fa-instagram"></i></a></li>
									<li><a class="he_social ico-youtube" href="https://www.youtube.com/channel/UCb_AfHCuOZTCEc7MLmrvwDQ" target="_blank"><i class="fa fa-youtube"></i></a></li>
									<li><a class="he_social ico-flickr" href="https://www.flickr.com/photos/tulasinstitute/" target="_blank"><i class="fa fa-flickr"></i></a></li>
								<!--	
									<li><a class="he_social ico-dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>	
									<li><a class="he_social ico-dropbox" href="#"><i class="fa fa-dropbox"></i></a></li>
									<li><a class="he_social ico-skype" href="#"><i class="fa fa-skype"></i></a></li>
									<li><a class="he_social ico-tumblr" href="#"><i class="fa fa-tumblr"></i></a></li>
									<li><a class="he_social ico-vimeo" href="#"><i class="fa fa-vimeo-square"></i></a></li>
									<li><a class="he_social ico-github" href="#"><i class="fa fa-github-alt"></i></a></li>
									<li><a class="he_social ico-renren" href="#"><i class="fa fa-renren"></i></a></li>
									<li><a class="he_social ico-vk" href="#"><i class="fa fa-vk"></i></a></li>
									<li><a class="he_social ico-xing" href="#"><i class="fa fa-xing"></i></a></li>
									<li><a class="he_social ico-weibo" href="#"><i class="fa fa-weibo"></i></a></li>
									<li><a class="he_social ico-rss" href="#"><i class="fa fa-rss"></i></a></li>										
								-->										
								</ul>
							</div>	 <!-- End Footer Social Icons -->	
							
						</div>
					</header>	<!-- End Header -->	
									
					<div class="row">
										
						<!-- Intro Section Description -->		
						<div id="intro_description" class="col-sm-7 col-md-7">
						
							<!-- Intro Section Title -->
							<h1 style="font-size:33px;">Admissions Open for B.Sc. Agriculture & Forestry 2018 Batch</h1>								
							<!-- Description #1 -->	
							<div class="intro_feature hidden-xs hidden-sm">
								<h4><i class="fa fa-check"></i> Industry Oriented</h4>
								<p class="hidden-xs hidden-sm" style="text-align:justify;">An optimal mix of learning methods to ensure personality development of the students, theoretical learning to real-life scenarios by advent of a unique corporate interface. </p>
							</div>
							
							<!-- Description #2 -->	
							<div class="intro_feature hidden-xs hidden-sm">
								<h4><i class="fa fa-check"></i> Right Student at Right Job</h4>
								<p class="hidden-xs hidden-sm" style="text-align:justify;">The institute ensures placements for its students by its association with 450+ core companies where they work as Research Officer, Quality Assurance Officer, Agriculture Officer,etc.</p>
							</div>
							
							<!-- Description #3 -->	
							<div class="intro_feature hidden-xs hidden-sm">
								<h4><i class="fa fa-check"></i> Creative to be innovative</h4>
								<p class="hidden-xs hidden-sm" style="text-align:justify;">Earning by practice through organising various events at the campus to innovate inherent skills to change the world</p>
							</div>
							
							<!-- Description #4 -->	
							<div class="intro_feature hidden-xs hidden-sm">
								<h4><i class="fa fa-check"></i> Infrastructure</h4>
								<p class="hidden-xs hidden-sm" style="text-align:justify;">Campus is spread over 20 acres of lush green environment with state of art auditorium, high-tech computer labs and with world class library having a collection of 32,000 books</p>
							</div>
															
						</div>	<!-- End Intro Section Description -->	
						
							
						<!-- Intro Section Form -->		
						<div id="intro_form" class="col-sm-5 col-md-5">
						
							<!--Register form -->
							<div class="form_register">
								<div id="user_data" style="">
									<h2> Apply Now! </h2>								
									<form action="" method="post" data-toggle="validator" role="form" id="mainform">									
										<div id="input_name" class="col-md-12">
											<input id="name" class="form-control" type="text" name="name" placeholder="Full Name" value="" required> 
										</div>											
										<div id="input_email" class="col-md-12">
											<input id="email" class="form-control" type="email" name="email" placeholder="Email Address" value="" required>  
										</div>										
										<div id="input_phone" class="col-md-12">
											<input id="phone" class="form-control" type="tel" name="phone" data-minlength="10" pattern="^[0-9]{1,}$" maxlength="10" placeholder="10 digit mobile no." value="" required> 
										</div>										
										<div id="input_state" class="col-md-12">
											<select id="state" name="state" class="form-control" required><option value="" selected>Select State</option><option value="35" >Andaman &amp; Nicobar Islands</option><option value="2">Andhra Pradesh</option><option value="25">Arunachal Pradesh</option><option value="4">Assam</option><option value="5">Bihar</option><option value="29">Chandigarh</option><option value="6">Chattisgarh</option><option value="10">Dadra &amp; Nagar Haveli</option><option value="9">Daman &amp; Diu</option><option value="7">Delhi</option><option value="20">Goa</option><option value="8">Gujarat</option><option value="11">Haryana</option><option value="12">Himachal Pradesh</option><option value="13">Jammu &amp; Kashmir</option><option value="14">Jharkhand</option><option value="15">Karnataka</option><option value="16">Kerala</option><option value="17">Lakshadweep</option><option value="18">Madhya Pradesh</option><option value="19">Maharashtra</option><option value="21">Manipur</option><option value="26">Meghalaya</option><option value="22">Mizoram</option><option value="23">Nagaland</option><option value="37">Nepal</option><option value="27">Odisha</option><option value="3">Pondicherry</option><option value="28">Punjab</option><option value="30">Rajasthan</option><option value="36">Sikkim</option><option value="31">Tamil Nadu</option><option value="24">Tripura</option><option value="32">Uttar Pradesh</option><option value="33">Uttarakhand</option><option value="34">West Bengal</option></select>
										</div>										
										<div id="input_course" class="col-md-12">
											<select id="course" name="course" class="form-control" required><option value="" selected>Select Course</option><option value="B.Sc - Agriculture">BSc Agriculture</option><option value="B.Sc - Forestry">BSc Forestry</option></select>
										</div>													
										<div id="form_register_btn" class="text-center">
											<input class="btn btn-primary btn-lg" type="submit" value="Submit" id="submit">
										</div>										
										<input type="hidden" name="country_code" value="91" />
										<input type="hidden" name="form" value="Landing Page Form" />
										<input type="hidden" name="request" value="remote/campaign" />
										<input type="hidden" name="account" value="3" />
										<input type="hidden" name="utm_source" value="<?php echo $_GET['utm_source'] ?>" />
										<input type="hidden" name="utm_medium" value="<?php echo $_GET['utm_medium'] ?>" />
										<input type="hidden" name="utm_campaign" value="<?php echo $_GET['utm_campaign'] ?>" />					
									</form>	
								</div>
								<div id="verify_otp" style="height:425px;display:none;">
									<h4 align="center"><b>Verify your mobile no.</b></h4>
									<form action="" method="post" data-toggle="validator" role="form">
										<div id="input_otp" class="col-md-12">
											<input type="text" name="otp" value="" size="40"  data-minlength="4" pattern="^[0-9]{1,}$" maxlength="4" class="form-control" id="name" placeholder="OTP" data-error="Incorrect OTP" required  />
											<div class="help-block with-errors"></div>
										</div>
										<div class="text-center"><center><input type="submit" value="Verify" class="btn btn-info" data-loading-text="Please wait..." /></center></div>
										<input type="hidden" name="country_code" value="91" />
										<input type="hidden" name="phone" value="" />
										<input type="hidden" name="request" value="remote/verify_mobile" />
										<input type="hidden" name="account" value="3" />
									</form>							
								</div>
								<div id="thank_you" style="height:425px;display:none;">
									<br /><p align="center"><span class="fa-stack fa-lg fa-3x"><i class="fa fa-thumbs-o-up fa-stack-2x"></i></span></p><br />
									
									<p style="color:#fff;font-size:15px;">Thank you for showing the interest in Tula's Institute. Till the time our counsellor will get back, you can go through the following important details.</p>	
										
									<a href="https://tulas.edu.in/" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-home fa-stack-1x fa-inverse"></i></span> <b style="color:#fff;">Click here for Main Website</b></a> <br />
									<a href="https://tulas.edu.in/infrastructure/hostel-and-mess/" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-building-o fa-stack-1x fa-inverse"></i></span> <b style="color:#fff;">Hostel Facilities in the Campus</b></a>	<br />
									<a href="https://tulas.edu.in/placements/" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-users fa-stack-1x fa-inverse"></i></span> <b style="color:#fff;">Placements in the College</b></a> <br />
									<a href="https://tulas.edu.in/events/" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-calendar fa-stack-1x fa-inverse"></i></span> <b style="color:#fff;">Recent Events in the College</b></a> <br />
									<a href="https://tulas.edu.in/wp-content/uploads/2017/05/Tulas-Prospectus-2017-1.pdf" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-download fa-stack-1x fa-inverse"></i></span> <b style="color:#fff;">Download Brochure</b></a> <br />
									
								</div>

							</div>							
						</div>	<!-- End Intro Section Form -->
					
					</div>	<!-- End row -->	
					
				</div>	   <!-- End container -->		
			</section>  <!-- END INTRO -->
			
			
			<!-- ABOUT-1
			============================================= -->
			
			<section id="about-1">
				<div class="container">	
				
					<!-- Section Title -->	
					<div class="row">
						<div class="col-md-12 titlebar">
							<h1>Some <strong>words about</strong> us</h1>
							<p>A stimulating curriculum with focus on sustainability, a tech savvy campus founded on state of the art infrastructure, and synergy with corporate right from its inception strengthens its efforts towards developing into a globally renowned centre of excellence in the field of education</p>
						</div>
					</div>
				
					<div class="row">
					
						<!--  About-1 Text -->	
						<div id="about-1-text" class="col-md-6">	
						
							<h4>Who are we?</h4>
							
							<p style="text-align:justify;">Tula’s Institute was founded in 2006 by Rishabh Educational Trust. Our endeavour is to create a congenial atmosphere to nurture talent through the support and guidance of experience faculty with an access to state of the art infrastructure.</p>
														
							<!--  Accordion -->
							<div id="accordion_holder">	
								<h4>Why choose us?</h4>

								<ul class="accordion clearfix">
									
									<!-- Text #1 -->
									<li id="text_1">
										<a href="#text1">Placement</a>								
										<div>
											<p>More than 450 companies visited the campus with highest package of 16 Lac p.a.  Placements have been an integral part of the institute and focusing on getting BEST OF THE CORE COMPANIES to the campus assuring the students 100% placement assistance</p>
										</div>									
									</li>				
											
									<!-- Text #2 -->
									<li id="text_2">
										<a href="#text2">Industry experts</a>								
										<div>
											<p>Various Workshops and Seminars are conducted where Industry Experts guide and mentor the students in understanding various niche related to the subject.</p>
										</div>									
									</li>
											
									<!-- Text #3 -->
									<li id="text_3">
										<a href="#text3">Digital Library</a>								
										<div>
											<p>Digital library has around 3200 e-books available round the clock through a FTP server on the intranet, whereby students can access the material and books anytime from anywhere on the campus</p>
										</div>									
									</li>
									
									<!-- Text #4 -->
									<li id="text_4">
										<a href="#text4">Faculty</a>								
										<div>
											<p>Tula's Institute has competitive faculty members, on whose shoulders lies the responsibility of taking their students to greater heights. The faculty and the student ratio is 12:1, which helps students to get MENTORED AND CATERED EFFECTIVELY. </p>
										</div>									
									</li>
									
									
								</ul>	
								
							</div>	<!--  End Accordion -->
							
						</div>	<!-- End About-1 Text --> 
						
						<!-- About-1 Image --> 
						<div id="about-1-img" class="col-md-6 text-center">
							<img class="img-responsive" src="img/generic.jpg" alt="image" />		
						</div>
					
					</div>	<!-- End row -->	
				</div>	   <!-- End container -->		
			</section>  <!-- END ABOUT-1 -->
			
			
			<!-- ABOUT-2
			============================================= -->
			
			<section id="about-2">
				<div class="container">					
					<div class="row">
						<div class="col-md-12" style="padding-bottom:30px;">

							<h4>Our Vision</h4>
							
							<p style="text-align:justify;">We envisage creating a premier educational institute committed to making young minds proficient enough to explore new and uncharted avenues with strong conviction and care. Our endeavour at Tula’s is to be recognized for excellence in nurturing and creating well-informed and responsible young professionals to assist in building the nation as responsible stakeholders</p>
						</div>	
							
						<!-- Embedded Video -->	
						<div id="video_holder" class="col-md-6">
							<h4>Discover more about us</h4>
							
							<div class="video-block">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/O3LejI7VCDs" frameborder="0" allowfullscreen></iframe>
							</div>						
						</div>
						
						<!--  About-2 Text -->	
						<div id="about-2-text" class="col-md-6">	
							
							<!--  Quote -->
							<div id="quote_holder" style="margin-top:0;">	
								<h4>What they say about us!</h4>
								
								<!-- Rotator Content -->
								<div class="flexslider">											
									<ul class="slides">
															
										<!--Testimonial #1 -->
										<li>
											<div class="testimonials">
												<blockquote class="quote-text">
												  <a href="#">My experience at Tula's Institute is the best so far. Along with the academics, I got to learn how to live up to the challenges and difficulties in any given situation, as here I got many opportunities to conduct different events at both cultural and technical fests. Now, that's education!</a>
												</blockquote>
												<img class="quote-author-avatar" src="https://tulas.edu.in/wp-content/uploads/2017/03/1-1-1.jpg" alt="">
												<p class="quote-author">
												  <a href="#"><strong>Anupam Vijay</strong></a>
												</p>											
											</div>
										</li>
																											
										<!--Testimonial #2 -->	
										<li>
											<div class="testimonials">
												<blockquote class="quote-text">
												  <a href="#">Tula’s Institute has been a place where I found a unique blend of excellence and commitment. Great faculties, wonderful ambience to learn and excellent facilities is how I describe this institution. All thanks to Tula’s for shaping up my career</a>
												</blockquote>
												<img class="quote-author-avatar" src="https://tulas.edu.in/wp-content/uploads/2017/03/2-1.jpg" alt="">
												<p class="quote-author">
												  <a href="#"><strong>Kumar Navjeet</strong></a>
												</p>											
											</div>
										</li>
																											
										<!--Testimonial #3 -->
										<li>
											<div class="testimonials">
												<blockquote class="quote-text">
												  <a href="#">I feel myself lucky to be the part of Tula family where at every step we have our faculty members to support us ,to guide us ,to correct us. Jai hind! Jai Tula's</a>
												</blockquote>
												<img class="quote-author-avatar" src="https://tulas.edu.in/wp-content/uploads/2017/03/3.jpg" alt="">
												<p class="quote-author">
												  <a href="#"><strong>Payal Singh</strong></a>
												</p>											
											</div>
										</li>
										
										<!--Testimonial #4 -->
										<li>
											<div class="testimonials">
												<blockquote class="quote-text">
												  <a href="#">Studying at Tulas is an all-round experience. Many times you need an environment to introspect and reflect on your own strengths and capabilities so that you align yourself to your interests, build on your abilities and internalize forward looking concepts. Tulas Institute provides this environment with a well-structured program, pedagogy, diverse culture & team of colleagues bringing rich experience from varied fields. Learning is not just from lecture room but from all the avenues that we get to interact with faculty, students and industry leaders.</a>
												</blockquote>
												<img class="quote-author-avatar" src="https://tulas.edu.in/wp-content/uploads/2017/03/4.jpg" alt="">
												<p class="quote-author">
												  <a href="#"><strong>Saurav Bisht</strong></a>
												</p>											
											</div>
										</li>
									</ul>												
								</div>	 <!-- End Rotator Content -->
								
							</div>	<!-- End Quote -->

						</div>	<!-- End About-2 Text --> 
					
					</div>	<!-- End row -->			
				</div>	  <!-- End container -->		
			</section>  <!-- END ABOUT-2 -->
			
			
			<!-- FEATURES
			============================================= -->
			
			<section id="features" class="parallax">
				<div class="container">	
				
					<!-- Section Title -->	
					<div class="row">
						<div class="col-md-12 titlebar">
							<h1>Our <strong>achievements</strong></h1>
							<p style="color:black;">Tula's Institute is committed to have the best infrastructure and facilities so as to ensure standards of excellence and supreme quality for the students. We have one of the most advanced and state-of-the-art campus to facilitate the process of acquiring knowledge for the students.</p>
						</div>
					</div>
				
					<!-- Features Holder -->	
					<div class="row">
						<div class="col-md-12">		
						
							<ul>
								<!-- Feature Icon 1 -->
								<li id="feature_1">       									
									<div class="col-sm-6 col-md-6 feature-box clearfix">	
										<div class="feature-box-icon">
											<i class="fa fa-trophy"></i>
										</div>
												
										<div class="feature-box-content">
											<h4>Global League Institute Award</h4>
											<p>for the 2nd time by the Great Place to Study Research Institute at House of Commons, London</p>
										</div>
									</div>
								</li>
								
								<!-- Feature Icon 2 -->
								<li id="feature_2">       									
									<div class="col-sm-6 col-md-6 feature-box clearfix">	
										<div class="feature-box-icon">
											<i class="fa fa-trophy"></i>
										 </div>
										 
										<div class="feature-box-content">
											<h4>Institute with Best Placement Award</h4>
											<p>Achieved this award at the prestigious 7th Annual Education Awards 2017, organized by Franchise India</p>
										</div>
									</div>
								</li>
								
								
								<!-- Feature Icon 3 -->
								<li id="feature_3">       
									<div class="col-sm-6 col-md-6 feature-box clearfix">	
										<div class="feature-box-icon">
											<i class="fa fa-trophy"></i>
										</div>
										
										<div class="feature-box-content">
											<h4>Education Evangelist of India</h4>
											<p>Tula’s was amongst the 17 legendary individuals to receive the accolade of Education Evangelist of India and awarded by SkillTree</p>
										</div>
									</div>
								</li>
																
								<!-- Feature Icon 4 -->
								<li id="feature_4">
									<div class="col-sm-6 col-md-6 feature-box clearfix">	
										<div class="feature-box-icon">
											<i class="fa fa-trophy"></i>
										</div>
										
										<div class="feature-box-content">
											<h4>APJ Kalam Award of Excellence</h4>
											<p>Founder & Chairman Shri Sunil Kumar Jain is awarded with the most prestigious award Dr APJ Abdul Kalam Excellence Award for Outstanding Individual Achievements & distinguished services to the nation</p>
										</div>							
									</div>
								</li>
								
							</ul>
						
						</div>	 <!-- End col-md-12 -->
					</div>	  <!-- End Features Holder -->	
					
				</div>	   <!-- End container -->		
			</section>  <!-- END FEATURES -->
			
			
			<!-- FAQs
			============================================= -->
			
			<section id="faq">
				<div class="container">	
				
					<!-- Section Title -->	
					<div class="row">
						<div class="col-md-12 titlebar">
							<h1>Frequently <strong>asked questions</strong></h1>
						</div>
					</div>
				
					<div class="row">
					
						<!-- Question #1-->
						<div id="question_1" class="col-md-6">	
							<div class="question">
								<h4>What is the admission procedure in Tula’s Institute?</h4>
								<ul style="text-align:justify;"><li>Applicant can download form online from website of Tula’s Institute and submit it to the respective mail Id</li><li>Courier the admission form along with required documents</li></ul>
							</div>							
						</div>
						
						<!-- Question #2-->
						<div id="question_2" class="col-md-6">							
							<div class="question">
								<h4>How are the extra curriculum activities for the students?</h4>
								<p style="text-align:justify;">There are lots of activities for student’s personal improvement and grooming apart from the studies. Students get to do various events in the college which benefit them to face the real world. For more information visit Tula’s Institute website. </p>
							</div>
						</div>
						
					</div>
					
					<div class="row">
					
						<!-- Question #3-->
						<div id="question_3" class="col-md-6">	
							<div class="question">
								<h4>What are the Eligibility Criteria for Admission?</h4>
								<p style="text-align:justify;">Minimum of 45% marks in Physics, Chemistry, Mathematics and those who have appeared in the JEE (Joint Entrance Examination).</p>
							</div>							
						</div>
						
						<!-- Question #4-->
						<div id="question_4" class="col-md-6">							
							<div class="question">
								<h4>What is the fee structure of Degree courses at Tula’s Institute?</h4>
								<p style="text-align:justify;">Tula’s Institute  has a variety of courses available and so the fees are different for each course. Please see the Course details wherein all the courses are addressed with their fees tuition fees applicable.</p>
							</div>
						</div>
						
					</div>	<!-- End row -->						
				
					<div class="row">
					
						<!-- Question #5-->
						<div id="question_5" class="col-md-6">	
							<div class="question">
								<h4>Is Scholarship available at Tula’s?</h4>
								<p style="text-align:justify;">We don’t provide any fixed amount of Scholarship to the students, it solely deserves on the students that if they are meritorious enough to get one,we would be happy to give one.</p>
							</div>							
						</div>
						
						<!-- Question #6-->
						<div id="question_6" class="col-md-6">	
							<div class="question">
								<h4>How to reach Tula’s Institute?</h4>
								<p style="text-align:justify;">Tula’s Institute is situated in Dehradun, the capital city of the state of Uttarakhand in the northern part of India. Dehradun is in the Doon Valley on the foothills of the Himalayas nestled between the river Ganges on the east and the river Yamuna on the west. Address:Tula’s Institute, The Engineering & Management College, Dhoolkot, PO-Selaqui, Chakrata Road, Dehradun-248011, (Uttarakhand)</p>
							</div>							
						</div>
						
					</div>	<!-- End row -->
		
				</div>	   <!-- End container -->		
			</section>  <!-- END FAQs -->
			

			<!-- CLIENTS
			============================================= -->
			
			<section id="clients" class="parallax">
				<div class="container">	
					
					<!-- Section Title -->	
					<div id="clients-titlebar" class="row">
						<div class="col-sm-12 ">
						
							<div class="titlebar">	
								<h1>Some of <strong>our recruiters</strong></h1>
								
							</div>

						</div>	
					</div>	<!-- End Section Title -->	
						
					<!-- Clients Carousel -->
					<div id="clients-holder" class="row">
						<div class="col-md-12">

							<!-- Clients Carousel-->
							<div id="our-customers" class="owl-carousel">
									
								<!-- Client Logo #1-->
								<div id="client_logo_1" class="item">
									<img class="img-responsive img-customer" src="img/thumbs/1.png">
								</div>
									
								<!-- Client Logo #2-->
								<div id="client_logo_2" class="item">
									<img class="img-responsive img-customer" src="img/thumbs/2.png">
								</div>
									
								<!-- Client Logo #3-->
								<div id="client_logo_3" class="item">
									<img class="img-responsive img-customer" src="img/thumbs/3.png">
								</div>
									
								<!-- Client Logo #4-->
								<div id="client_logo_4" class="item">
									<img class="img-responsive img-customer" src="img/thumbs/4.png">
								</div>
									
								<!-- Client Logo #5-->
								<div id="client_logo_5" class="item">
									<img class="img-responsive img-customer" src="img/thumbs/5.png">
								</div>
									
								<!-- Client Logo #6-->
								<div id="client_logo_6" class="item">
									<img class="img-responsive img-customer" src="img/thumbs/6.png">
								</div>
								
							</div>	<!-- End Clients Carousel-->							  
					
						</div>
					</div>	   <!-- End Clients Holder -->	
					
				</div>	   <!-- End container -->		
			</section>  <!-- END CLIENTS -->
			
			
			<!-- CALL TO ACTION
			============================================= -->
			
			<section id="call-to-action" class="parallax">
				<div class="container">	
					<div class="row">
					
						<!-- Call To Action Content -->	
						<div class="col-sm-12 text-center">
						
							<h1>Download Brochure</h1>
							<p>Brochure contains information about college Facilities, Placements, Awards, Student Clubs and Admission.  </p>
							
							<!-- Call To Action Button -->	
							<a class="btn btn-primary btn-lg" href="#top" id="download_brochure">Download Brochure</a>
							
						</div>	<!-- End Call To Action Content -->	
					
					</div>	<!-- End row -->	
				</div>	   <!-- End container -->		
			</section>  <!-- END CALL TO ACTION -->
			
			
			<!-- FOOTER
			============================================= -->
			
			<footer id="footer">
				<div class="container">	
					<div class="row">
										
						<!-- Footer Navigation Menu -->
						<div id="footer_nav" class="col-sm-6 col-md-6">
							<ul class="footer-nav clearfix">
								<li><a href="tel:+91-7579415180">Admission Helpline: +91-7579415180</a></li>
							</ul>

							<div id="footer_copy">
								<p>&copy; Copyright 2017 <span>Tula's Institute</span> All Right Reserved</p>
							</div>							
						</div>	<!-- End Footer Navigation Menu -->
						
												
						<!-- Footer Social Icons -->
						<div id="footer_icons" class="col-sm-6 col-md-6 text-right">																	
							<ul class="footer-socials clearfix">
								<li><a class="foo_social ico-facebook" href="https://www.facebook.com/Tulas.Institute.Dehradun/" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a class="foo_social ico-twitter" href="https://twitter.com/Tulas_Dehradun" target="_blank"><i class="fa fa-twitter"></i></a></li>	
								<li><a class="foo_social ico-google-plus" href="https://plus.google.com/u/0/b/117590320289532314094/109874494989346734056" target="_blank"><i class="fa fa-google-plus"></i></a></li>
								<li><a class="foo_social ico-linkedin" href="http://www.linkedin.com/company/1671832" target="_blank"><i class="fa fa-linkedin"></i></a></li>
								<li><a class="foo_social ico-instagram" href="https://www.instagram.com/tulasdehradun/" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<li><a class="foo_social ico-youtube"  href="https://www.youtube.com/channel/UCb_AfHCuOZTCEc7MLmrvwDQ" target="_blank"><i class="fa fa-youtube"></i></a></li>
								<li><a class="foo_social ico-flickr" href="https://www.flickr.com/photos/tulasinstitute/" target="_blank"><i class="fa fa-flickr"></i></a></li>
								<li><a class="foo_social ico-pinterest" href="https://in.pinterest.com/tulasinstitute/" target="_blank"><i class="fa fa-pinterest"></i></a></li>			
							<!--									
									
								<li><a class="foo_social ico-dropbox" href="#"><i class="fa fa-dropbox"></i></a></li>
								<li><a class="foo_social ico-skype" href="#"><i class="fa fa-skype"></i></a></li>
								<li><a class="foo_social ico-tumblr" href="#"><i class="fa fa-tumblr"></i></a></li>
								<li><a class="foo_social ico-vimeo" href="#"><i class="fa fa-vimeo-square"></i></a></li>
								<li><a class="foo_social ico-github" href="#"><i class="fa fa-github-alt"></i></a></li>
								<li><a class="foo_social ico-renren" href="#"><i class="fa fa-renren"></i></a></li>
								<li><a class="foo_social ico-vk" href="#"><i class="fa fa-vk"></i></a></li>
								<li><a class="foo_social ico-xing" href="#"><i class="fa fa-xing"></i></a></li>
								<li><a class="foo_social ico-weibo" href="#"><i class="fa fa-weibo"></i></a></li>
								<li><a class="foo_social ico-rss" href="#"><i class="fa fa-rss"></i></a></li>										
							-->										
							</ul>
						</div>	 <!-- End Footer Social Icons -->
						
						
											
					</div>	<!-- End row -->						
				</div>	  <!-- End container -->		
			</footer>	<!-- END FOOTER -->
			
		
		</div>	<!-- END CONTENT WRAPPER -->
	
	
		<!-- EXTERNAL SCRIPTS
		============================================= -->
		
		<script src="js/jquery-2.1.0.min.js" type="text/javascript"></script>
		<script src="js/bootstrap.min.js" type="text/javascript"></script>	
		<script src="js/retina.js" type="text/javascript"></script>	
		<script src="js/modernizr.custom.js" type="text/javascript"></script>	
		<script src="js/jquery.easing.js" type="text/javascript"></script>
		<script src="js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
		<script defer src="js/jquery.flexslider.js" type="text/javascript"></script>
		<script src="js/jquery.accordion.source.js" type="text/javascript"></script>	
		<script src="js/owl.carousel.js" type="text/javascript"></script>
		<script src="js/waypoints.min.js" type="text/javascript"></script>	
		<script src="js/animations.js" type="text/javascript"></script>		
		<script src="js/validator.js" type="text/javascript"></script>
		<script src="js/custom.js" type="text/javascript"></script>
		

		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		

				<!-- Global site tag (gtag.js) - AdWords: 966813215 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-966813215"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-966813215');



  gtag('event', 'page_view', {
    'send_to': 'AW-966813215',
    'edu_pagetype': '<?php echo $_GET["edu_pagetype"]; ?>',
    'edu_pid': '<?php echo $_GET["edu_pid"]; ?>',
    'edu_plocid': '<?php echo $_GET["edu_plocid"]; ?>',
    'edu_totalvalue': '<?php echo $_GET["edu_totalvalue"]; ?>',
    'user_id': '<?php echo $_GET["user_id"]; ?>'
  });
</script>
			
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37986966-1', 'auto');
  ga('send', 'pageview');
</script>
					
					
<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 959625469;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/959625469/?guid=ON&amp;script=0"/>
</div>
</noscript>		
<!--Start of Intercom Script-->
<script>
  window.intercomSettings = {
    app_id: "lzmpqie9"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/lzmpqie9';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<!—End of Intercom Script-->
	</body>

</html>
